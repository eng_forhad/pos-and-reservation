from django.urls import path
from adminapp import views
from .views import RoleAPIView, RoleDetailsAPIView

urlpatterns = [
    
    path('role/', views.RoleAPIView.as_view(), name = "r"),
    path('role/<int:id>', views.RoleDetailsAPIView.as_view(), name = "r"),
]
