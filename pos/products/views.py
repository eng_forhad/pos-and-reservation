from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from products.serializers import ProductCategorySerilizer , ProductSerilizer
from products.models import ProductCategory, Products
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated


class ProductListApiView(ListCreateAPIView):
    serializer_class = ProductSerilizer
    queryset = Products.objects.all()
    # permissions_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return self.queryset.all()

class ProductDetailsAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProductSerilizer
    queryset = Products.objects.all()
    permissions_classes = (IsAuthenticated,)
    lookup_field = "id"

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return Products.objects.all()



class ProductCategoryListApiView(ListCreateAPIView):
    serializer_class = ProductCategorySerilizer
    queryset = ProductCategory.objects.all()
    # permissions_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return self.queryset.all()

class ProductCategoryDetailsAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProductCategorySerilizer
    queryset = ProductCategory.objects.all()
    permissions_classes = (IsAuthenticated,)
    lookup_field = "id"

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return Products.objects.all()