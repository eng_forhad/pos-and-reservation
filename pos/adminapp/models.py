from django.db import models

# Create your models here.


class Roles(models.Model):
    name = models.CharField(max_length=100)
    status = models.BooleanField(default=False)

    class Meta:
        db_table = 'roles'

