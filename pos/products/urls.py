from django.urls import path
from products.views import ProductListApiView, ProductDetailsAPIView , ProductCategoryListApiView, ProductCategoryDetailsAPIView
urlpatterns = [
    path('', ProductListApiView.as_view(), name = "product"),
    path('<int:id>', ProductDetailsAPIView.as_view(), name = "product"),
    path('category/', ProductCategoryListApiView.as_view(), name = "productCategory"),
    path('category/<int:id>', ProductCategoryDetailsAPIView.as_view(), name = "ProductCategory"),
]
