from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView
from .serializer import RoleSerializer
from .models import Roles
from rest_framework.permissions import IsAuthenticated

class RoleAPIView(ListCreateAPIView):
    serializer_class = RoleSerializer
    queryset = Roles.objects.all()
    permissions_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return self.queryset.all()

class RoleDetailsAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RoleSerializer
    queryset = Roles.objects.all()
    permissions_classes = (IsAuthenticated,)
    lookup_field = "id"

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        return Roles.objects.all()

 