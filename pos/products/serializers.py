from rest_framework import serializers
from products.models import ProductCategory, Products

class ProductCategorySerilizer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategory        
        fields = '__all__'

class ProductSerilizer(serializers.ModelSerializer):
    
    class Meta:
        model = Products        
        fields = '__all__'